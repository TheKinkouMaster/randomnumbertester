﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderScript : MonoBehaviour
{
    public Slider m_slider;
    public Text m_textName;
    public Text m_textvalue;
    public int m_value;
    public int max;
    public int min;
    public int m_name;
    private void Update()
    {
        if(max > 0) m_slider.value = (float)(m_value-min)/(max-min);
        m_textvalue.text = m_value.ToString();
    }
}
