﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    public GameObject m_sliderPrefab;
    public GameObject m_canvasAnchor;
    public List<SliderScript> m_sliders;
    public float m_yjump;

    public int m_minNumber;
    public int m_maxNumber;
    public int m_trynumber;
    public int m_currentTry;
    public RNG m_RNG;
    public int triesPerFrame;

    public void Awake()
    {
        M_setupSliders();
    }
    private void Update()
    {
        for (int i = 0; i < triesPerFrame; i++)
        {
            if (m_currentTry >= m_trynumber) return;
            m_currentTry++;
            M_RollNumber();
            M_CalculateValues();
        }
    }
    public int M_CheckHighest()
    {
        int retval = 0;
        foreach (SliderScript obj in m_sliders) if (obj.m_value > retval) retval = obj.m_value;
        return retval;
    }
    public int M_CheckMinimal()
    {
        int retval = M_CheckHighest();
        foreach (SliderScript obj in m_sliders) if (obj.m_value < retval) retval = obj.m_value;
        return retval;
    }
    public void M_RollNumber()
    {
        
        int number = m_RNG.Number(m_minNumber, m_maxNumber);
        Debug.Log(number);
        m_sliders[number].m_value++;
        foreach (SliderScript obj in m_sliders) obj.max = M_CheckHighest();
        foreach (SliderScript obj in m_sliders) obj.min = M_CheckMinimal();
    }
    public void M_setupSliders()
    {
        for (int i = 0; i < ((m_maxNumber - m_minNumber) + 1); i++)
        {
            GameObject obj = Instantiate(m_sliderPrefab);
            obj.transform.SetParent(m_canvasAnchor.transform);
            obj.transform.localPosition = new Vector3(200, -(i * m_yjump) - 50, 0);
            obj.transform.localScale = new Vector3(1, 1, 1);
            m_sliders.Add(obj.GetComponent<SliderScript>());
            obj.GetComponent<SliderScript>().m_name = (i + m_minNumber);
            obj.GetComponent<SliderScript>().m_textName.text = (i + m_minNumber).ToString();
        }
    }

    public Text m_srednia;
    public Text m_mediana;
    public Text m_odchylenie;

    public void M_CalculateValues()
    {
        List<int> values = new List<int>();
        float srednia = 0;
        foreach (SliderScript obj in m_sliders)
            for(int i = 0; i < obj.m_value; i++)
                values.Add(obj.m_name);
        foreach (int obj in values)
            srednia = (srednia + obj);
        srednia = srednia / values.Count;
        m_srednia.text = srednia.ToString();

        m_mediana.text = values[values.Count / 2].ToString();
    }

    public Text m_spread10;
    public Text m_spread100;
    public Text m_spread;
}

