﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicRNG : RNG
{
    public override int Number(int min, int max)
    {
        int range = max - min;
        range++;

        float seed = Random.value;
        seed = range * seed;
        int retval = Mathf.FloorToInt(seed);
        if (retval == max + 1) retval = max;
        return retval + min;
    }
}
